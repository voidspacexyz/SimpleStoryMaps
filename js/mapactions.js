import config from './config.js';

let categoriesColor;
let myStaticData;
let markersLayerGroup;

async function loadSelfHostedData() {
  try {
    const [categoriesResponse, geoJsonResponse] = await Promise.all([
      fetch(config.categoriesUrl),
      fetch(config.geoJsonUrl)
    ]);

    const [categoriesData, geoJsonData] = await Promise.all([
      categoriesResponse.json(),
      geoJsonResponse.json()
    ]);

    categoriesColor = categoriesData;
    myStaticData = geoJsonData;

    initMap();
  } catch (error) {
    console.error('Error loading data:', error);
  }
}
function convertGSheetCategoryToCategories(arr) {
  const output = {};
  arr.forEach((item) => {
    const categoryName = item["Category Name"];
    const categoryColour = item["Category Colour"];
    output[categoryName] = categoryColour;
  });
  return output;
}
function convertGSheetDatatoGeoData(arr){

  const output = {};
  output["type"] = "FeatureCollection",
  output["features"] = []
  arr.forEach((item) => {
    const coordinates = item["GPS"]
    const category = item["Category"]
    const popupContent = item["Description"]
    const location = item["Location"]
    const tags = item["Tags"]
    const date = item["Date"]
    const datapoint = {}
    datapoint["type"] = "Feature"
    datapoint["geometry"] = {}
    datapoint["geometry"]["type"] = "Point"
    datapoint["geometry"]["coordinates"] = coordinates 
    datapoint["properties"] = {}
    datapoint["properties"]["category"] = category 
    datapoint["properties"]["popupContent"] = popupContent 
    datapoint["properties"]["datetime"] = date 
    datapoint["properties"]["location"] = location 
    datapoint["properties"]["tags"] = [tags.split(",")]
    output["features"].push(datapoint)
  })
  return output
}

function loadGoogleSheetData(){
  var id = config.sheetID
  var dataSheet = "Data"
  var categorySheet = "Categories"
  const dataSheetOptions = { sheetName: 'Data', useFormat: true }
  const categorySheetOptions = { sheetName: 'Categories', useFormat: true }
  const categoryParser = new PublicGoogleSheetsParser(id, categorySheetOptions)
  const dataParser = new PublicGoogleSheetsParser(id, dataSheetOptions)
  categoryParser.parse().then((categories) => {
	  categoriesColor = convertGSheetCategoryToCategories(categories)
    })
  dataParser.parse().then((data) => {
    myStaticData = convertGSheetDatatoGeoData(data)
myStaticData.features.forEach(feature => {
})
  initMap();
  })
}


function initMap() {
  const map = L.map('map').setView(config.mapCenter, config.mapZoom);
  markersLayerGroup = L.layerGroup().addTo(map);
  
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  function getVideoExtension(url) {
    const extension = url.split('.').pop().toLowerCase();
    return ['mp4', 'webm', 'ogv'].includes(extension) ? extension : 'mp4';
  }

  function onEachFeature(feature, layer) {
    let popupContent = ` ${feature.properties.popupContent}<br>
                          <strong>Date & Time:</strong> ${feature.properties.datetime}<br>
                          <strong>Location:</strong> ${feature.properties.location}<br>`;

    if (feature.properties.readmore) {
      popupContent += '<strong>Read more:</strong><br>';
      feature.properties.readmore.forEach((source, index) => {
        popupContent += `<a href="${source.url}" target="_blank">${source.name}</a>`;
        if (index < feature.properties.readmore.length - 1) {
          popupContent += ', '; // Add comma if it's not the last element
        } else {
          popupContent += '</br>'
        }
      });
    }
    

    if (feature.properties.video_url) {
      const videoExtension = getVideoExtension(feature.properties.video_url);
      popupContent += `<br><video class="video-js" width="320" height="240" controls data-setup='{}'>
                            <source src="${feature.properties.video_url}" type="video/${videoExtension}">
                            Your browser does not support the video tag.
                          </video>`;
    }
    if (feature.properties.youtube){
      var video_id = feature.properties.youtube.split("=")[1]
      popupContent += `<br>  <iframe width="320" height="240" src="https://www.youtube.com/embed/`+video_id+`" title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`;
    }
    


    
    if (feature.properties.tags) {
      popupContent += `<br><strong>Tags:</strong> ${feature.properties.tags.join(', ')}`;
    }
    layer.bindPopup(popupContent);
  }

  const filterData = () => {
    const startDate = document.getElementById('start-date').valueAsDate;
    const endDate = document.getElementById('end-date').valueAsDate;
    const categoryFilterValue = document.getElementById('category-filter').value || 'all';
    const yearFilterValue = document.getElementById('year-filter').value || 'all';

    const filteredData = myStaticData.features.filter(feature => {
      const featureDate = new Date(feature.properties.datetime);
      const isCategoryMatch = categoryFilterValue === 'all' || feature.properties.category === categoryFilterValue;
      if (startDate && endDate){
        const isWithinRange = (!startDate || featureDate >= startDate) && (!endDate || featureDate <= endDate);
        return isWithinRange && isCategoryMatch;
      } else {
        const isYearMatch = yearFilterValue === 'all' || featureDate.getFullYear() === parseInt(yearFilterValue);
        return isYearMatch && isCategoryMatch;
      }
    });

    updateMarkers(filteredData);
  };

  const createFilterOptions = (filterId, filterValues) => {
    const filterElement = document.getElementById(filterId);
    filterValues.forEach(value => {
      const option = document.createElement('option');
      option.value = value;
      option.textContent = value;
      filterElement.appendChild(option);
    });
  };

  const yearsSet = new Set(); // Use a set to store unique years

myStaticData.features.forEach(feature => {
  const dateParts = feature.properties.datetime.split('-');
  const year = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]).getFullYear();
  yearsSet.add(year); // Add each year to the set
});

const years = Array.from(yearsSet).sort((a, b) => b - a); // Sort the years in descending order

// Populate the year dropdown with the extracted unique year values
const yearFilter = document.getElementById('year-filter');
years.forEach(year => {
  const option = document.createElement('option');
  option.value = year;
  option.textContent = year;
  yearFilter.appendChild(option);
});


  const updateMarkers = (filteredData) => {
    if (markersLayerGroup) {
      map.removeLayer(markersLayerGroup);
    }

    markersLayerGroup = L.geoJSON(filteredData, {
      pointToLayer: function (feature, latlng) {
        return L.circleMarker(latlng, {
          radius: 10,
          color: '#000',
          fillColor: categoriesColor[feature.properties.category],
          fillOpacity: 0.8
        });
      },
      onEachFeature: onEachFeature
    });

    map.addLayer(markersLayerGroup);

    if (markersLayerGroup.getLayers().length > 0) {
      map.fitBounds(markersLayerGroup.getBounds());
    }
  };

  const yearFilterValues = Array.from(new Set(myStaticData.features.map(feature => new Date(feature.properties.datetime).getFullYear())));
  const categoryFilterValues = Object.keys(categoriesColor);

  createFilterOptions('year-filter', yearFilterValues);
  createFilterOptions('category-filter', categoryFilterValues);

  document.getElementById('year-filter').addEventListener('change', filterData);
  document.getElementById('category-filter').addEventListener('change', filterData);
  document.getElementById('start-date').addEventListener('change', filterData);
  document.getElementById('end-date').addEventListener('change', filterData);

  filterData();
}
if(config.dataFrom === "google_sheet"){
    loadGoogleSheetData();
  }else{
    loadSelfHostedData();
  
}
