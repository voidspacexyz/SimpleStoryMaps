const fs = require('fs');

const categories = [
  'Sample',
  'Data',
  'Showcase',
  'LocalNews',
  'AI',
  'Policy',
  'Government',
  'Others'
];

const generateRandomCoordinatesInIndia = () => {
  const lat = Math.random() * (37.08 - 6.75) + 6.75;
  const lng = Math.random() * (97.39 - 68.10) + 68.10;
  return [lng, lat];
};

const generateRandomDate = () => {
  const start = new Date(2010, 0, 1);
  const end = new Date();
  const date = new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = date.getFullYear();
  return `${day}/${month}/${year}`;
};

const generateRandomSources = () => {
  const sources = [
    {
      name: 'The Quint',
      url: 'https://thequint.com'
    },
    {
      name: 'The Hindu',
      url: 'https://thehindu.com'
    },
    {
      name: 'NDTV',
      url: 'https://ndtv.com'
    },
    {
      name: 'The Indian Express',
      url: 'https://indianexpress.com'
    },
    {
      name: 'The Times of India',
      url: 'https://timesofindia.indiatimes.com'
    }
  ];

  const randomIndex = Math.floor(Math.random() * sources.length);
  return [sources[randomIndex]];
};

const generateRandomTags = () => {
  const tags = [
    'sample',
    'data',
    'showcase',
    'local',
    'news',
    'ai',
    'policy',
    'government',
    'others'
  ];

  const randomTagsCount = Math.floor(Math.random() * 3) + 1;
  const randomTags = [];

  for (let i = 0; i < randomTagsCount; i++) {
    const randomIndex = Math.floor(Math.random() * tags.length);
    randomTags.push(tags[randomIndex]);
  }

  return randomTags;
};

const dataPoints = [];

for (let i = 0; i < 30; i++) {
  const coordinates = generateRandomCoordinatesInIndia();
  const category = categories[Math.floor(Math.random() * categories.length)];
  const datetime = generateRandomDate();
  const location = 'India';
  const sources = generateRandomSources();
  const tags = generateRandomTags();

  dataPoints.push({
    type: 'Feature',
    geometry: {
      type: 'Point',
      coordinates
    },
    properties: {
      category,
      popupContent: 'This is popup content',
      datetime,
      location,
      sources,
      tags
    }
  });
}

// Generate categories.json
const categoriesJson = JSON.stringify(categories.reduce((acc, category) => {
  acc[category] = '#' + Math.floor(Math.random()*16777215).toString(16);
  return acc;
}, {}), null, 2);

fs.writeFileSync('./sample/sample.categories.json', categoriesJson);

// Generate data.geojson
const dataGeoJson = JSON.stringify({
  type: 'FeatureCollection',
  features: dataPoints
}, null, 2);

fs.writeFileSync('./sample/sample.data.geojson', dataGeoJson);