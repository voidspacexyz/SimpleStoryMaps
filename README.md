# Simple Map Stories
Simple Map Stories (SMS) is a location centeric, time based data visualisation tool. In short, it keeps GPS as a center reference, to help you look at data over a period of time. For simpler understanding, keeping GPS as your X-Axis and Time as your Y-Axis.

## Features 
The tool, takes point-type geojson and plots it on a map. 

1. It supports Text popup along with a playable Audio/Video as its data points.   
2. Add and filter by categories
3. Filter data by year or a date range 
4. Colour coded categories
5. Static site, with data thta could be loaded from Google Sheet or GeoJson files


Note: [Gitlab repo](https://gitlab.com/voidspacexyz/simplestorymaps) is a readonly mirror. The primary codebase is on [Codeberg](https://codeberg.org/voidspacexyz/simplestorymaps). 


# How to setup this tool

1. Clone this repository into your root directory for the static site.
2. copy js/config.js.sample to js/config.js
3. Follow the [data loading instructions](#DataLoading) below based on your need. 
4. Write your nginx or apache or any webserver of your choice to render a static site. Below is an nginx sample for rendering the site as mapstories.example.com

    ```
    server {
        server_name mapstories.example.com;
        listen 80;
        root /path/to/project;
        index index.html;
        access_log /path/to/access.log;
        error_log /path/to/error.log;
    }
    ```
5. Run certbot for automatic ssl and your site is live.


Note: 
    - This tool is intentionally not built to use cool frameworks, and it will not be re-written in frameworks like React or Vue at any point in time.
    - We can absolutely deploy this site in gitlab or github pages or in PaaS platforms like Vercel or Netlify. Instructions for the same to be updated soon. 


## Data Loading 
### GeoJSON files as your data 
1. Refer to sample/ on how to write the respective json files if you want the data to be loaded as GeoJSON. *Google Sheet instructions to be updated soon*.
2. Create a directory called data/ and store your json files inside the same in case of self hosted.
3. Assuming your site is mapstories.example.com and your json files are placed in data/ directory, then your categories url will be mapstories.example.com/data/categories.json and data url will be mapstories.example.com/data/data.geojson

### Loading data from Google Sheets 
Instructions to be updated soon.
